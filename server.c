#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/signal.h>
#include <pthread.h>
#include <sys/select.h>

#define MESSAGE_SIZE 256
#define BUFFER_SIZE 32
#define MAIN_ROOM 1

struct client;
struct messagePack;

struct client {
  int fileDescriptor;
  int connectionNumber;
  struct sockaddr_in connectionAddress;
  int room;
  int firstMessage;
  struct client *previous;
  struct client *next;
  char *name;
  char *serverResponse;
};

struct messagePack {
  struct client *c;
  char *message;
};

void childend(int signo) { wait(NULL); }

int isCommand(char *msg) { return msg[0] == '/' ? 1 : 0; }


char *concatStrings(const char *s1, const char *s2) {
  char *result =
      malloc(strlen(s1) + strlen(s2) + 1);  // +1 for the null-terminator
  strcpy(result, s1);
  strcat(result, s2);
  return result;
}

void addClient(struct client **clients, struct client *clientToAdd) {
  struct client **pointer = clients;
  clientToAdd->next = NULL;

  if (*pointer == NULL) {
    clientToAdd->previous = NULL;
    *clients = clientToAdd;
  } else {
    while ((*pointer)->next != NULL) {
      pointer = &(*pointer)->next;
    }

    clientToAdd->previous = *pointer;
    (*pointer)->next = clientToAdd;
  }
}

void removeClient(struct client **clientToRemove, struct client **clients) {
  struct client **pClient = &(*clientToRemove)->previous;
  struct client **nClient = &(*clientToRemove)->next;

  // p -> O -> n
  if ((*clientToRemove)->previous != NULL) {
    (*clientToRemove)->previous->next = *nClient;
  } else {
    *clients = NULL;
  }

  if (*nClient != NULL) {
    (*clientToRemove)->next->previous = *pClient;
  }

  printf("User %s left...\n", (*clientToRemove)->name);
  close((*clientToRemove)->fileDescriptor);
  free((*clientToRemove)->name);
  free(*clientToRemove);
  *clientToRemove = NULL;
}

void showClients(struct client *clients) {
  struct client *p = clients;
  int counter = 1;
  while (p != NULL) {
    printf("%d. Klient id:%d nick:%s fd:%d\n", counter++, p->connectionNumber,
           p->name, p->fileDescriptor);
    p = p->next;
  }
}

void getAllClientsFromRoom(struct client *clients, char *messageBuffer,
                           int room) {
  char *temp = malloc(MESSAGE_SIZE * sizeof(char));
  memset(temp, 0, MESSAGE_SIZE);
  struct client *p = clients;
  int counter = 1;
  while (p != NULL) {
    if (p->room == room) {
      temp = concatStrings(temp, p->name);
      printf("%d. Klient id:%d nick:%s fd:%d\n", counter++, p->connectionNumber,
             p->name, p->fileDescriptor);
      if (p->next != NULL) temp = concatStrings(temp, ", ");
    }

    p = p->next;
  }

  strcpy(messageBuffer, temp);
  free(temp);
}

struct client *findClientByName(struct client *clients, char *clientName) {
  struct client *p = clients;
  while (p != NULL && p->name != NULL) {
    if (!strcmp(clientName, p->name)) return p;
    p = p->next;
  }

  return NULL;
}

struct client *findClientByFileDescriptor(struct client *clients,
                                          int clientFileDescriptor) {
  struct client *p = clients;
  while (p != NULL) {
    if (clientFileDescriptor == p->fileDescriptor) return p;
    p = p->next;
  }

  return NULL;
}

int readMessage(int fileDescriptorToReadFrom, char **pointerForMessage) {
  int totalReadLength = 0;
  int readLength = 0;

  while (totalReadLength < MESSAGE_SIZE) {
    readLength = read(fileDescriptorToReadFrom,
                      (*pointerForMessage) + totalReadLength, MESSAGE_SIZE);
    if (readLength <= 0) {
      return 0;
    }

    totalReadLength += readLength;
    if (strchr(*pointerForMessage, '\0')) break;
  }

  return 1;
}

int writeMessage(int fileDescriptorToWriteTo, char *messageToSend) {
  char *temporaryString = malloc(sizeof(char) * MESSAGE_SIZE);
  snprintf(temporaryString, MESSAGE_SIZE, "%s", messageToSend);
  int currentMessageSize = strlen(temporaryString);
  if (MESSAGE_SIZE < currentMessageSize) currentMessageSize = MESSAGE_SIZE;
  int totalWroteLength = 0;
  int wroteLength = 0;
  while (totalWroteLength < currentMessageSize) {
    wroteLength = write(fileDescriptorToWriteTo,
                        temporaryString + totalWroteLength, MESSAGE_SIZE);
    totalWroteLength += wroteLength;
    if (wroteLength <= 0) {
      free(temporaryString);
      return 0;
    }
  }

  free(temporaryString);
  return 1;
}

void *sendMessageToUser(void *arg) {
  struct messagePack *pack = (struct messagePack *)arg;
  if (!writeMessage(pack->c->fileDescriptor, pack->message)) {
    printf("%s nie dostal: '%s'\n", pack->c->name, pack->message);
  };
  free(pack->message);
  free(pack);
  return EXIT_SUCCESS;
}

void sendMessageToRoom(struct client *clients, int roomNumber, char *message,
                       int senderId) {
  pthread_t tid;
  char *temp;
  struct client *p = clients;
  struct messagePack *pack;
  while (p != NULL) {
    if (p->room == roomNumber && p->connectionNumber != senderId) {
      pack = malloc(sizeof(struct messagePack));
      temp = malloc(sizeof(char) * (strlen(message) + 1));
      strcpy(temp, message);
      pack->message = temp;
      pack->c = p;
      pthread_create(&tid, NULL, sendMessageToUser, pack);
      pthread_detach(tid);
    }

    p = p->next;
  }
}

int main(int argc, char **argv) {
  int connectionNumber = 0;
  socklen_t sid;
  struct sockaddr_in server_addr;
  char *messageBuffer = malloc(MESSAGE_SIZE * sizeof(char));
  char *tempString = malloc(MESSAGE_SIZE * sizeof(char));
  char *tempServerResponse = NULL;

  int socketDescriptor, i;
  struct client *clients = NULL;
  struct client *temporaryClient = NULL;
  struct client *newClient = NULL;
  int writeStatus, readStatus;
  int one = 1;
  server_addr.sin_family = PF_INET;
  server_addr.sin_port = htons(1234);
  server_addr.sin_addr.s_addr = INADDR_ANY;

  int clientFileDescriptor, maxFileDescriptor, availableFileDescriptors,
      returnedCount;
  static struct timeval timeOut;
  fd_set clientsForWrite, rmask, wmask, allClientsForRead;

  FD_ZERO(&allClientsForRead);
  FD_ZERO(&clientsForWrite);
  FD_ZERO(&rmask);
  FD_ZERO(&wmask);

  signal(SIGCHLD, childend);
  socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);
  setsockopt(socketDescriptor, SOL_SOCKET, SO_REUSEADDR, (char *)&one,
             sizeof(one));

  bind(socketDescriptor, (struct sockaddr *)&server_addr, sizeof(server_addr));
  listen(socketDescriptor, 15);

  maxFileDescriptor = socketDescriptor;

  while (1) {
    FD_SET(socketDescriptor, &allClientsForRead);
    wmask = clientsForWrite;
    rmask = allClientsForRead;
    timeOut.tv_sec = 30;
    timeOut.tv_usec = 0;
    returnedCount =
        select(maxFileDescriptor + 1, &rmask, &wmask, NULL, &timeOut);
    if (returnedCount == 0) {
      printf("timed out\n");
      continue;
    }

    availableFileDescriptors = returnedCount;
    if (FD_ISSET(socketDescriptor, &rmask)) {
      availableFileDescriptors -= 1;
      newClient = malloc(sizeof(struct client));
      newClient->name = NULL;
      sid = sizeof(newClient->connectionAddress);

      clientFileDescriptor =
          accept(socketDescriptor,
                 (struct sockaddr *)&newClient->connectionAddress, &sid);
      newClient->fileDescriptor = clientFileDescriptor;
      newClient->connectionNumber = ++connectionNumber;
      newClient->firstMessage = 1;
      newClient->serverResponse = NULL;
      printf("%d. New connection: %s:%d on FD:%d\n", connectionNumber,
             inet_ntoa((struct in_addr)newClient->connectionAddress.sin_addr),
             newClient->connectionAddress.sin_port, clientFileDescriptor);

      addClient(&clients, newClient);

      FD_SET(clientFileDescriptor, &allClientsForRead);
      FD_SET(clientFileDescriptor, &clientsForWrite);
      if (clientFileDescriptor > maxFileDescriptor)
        maxFileDescriptor = clientFileDescriptor;
    }

    for (i = socketDescriptor + 1;
         i <= maxFileDescriptor && 0 < availableFileDescriptors; i++) {
      temporaryClient = findClientByFileDescriptor(clients, i);
      if (FD_ISSET(i, &wmask)) {
        availableFileDescriptors -= 1;
        if (temporaryClient == NULL) {
          printf(
              "WOW, cos poszlo nie tak! Nie ma takiego clienta po "
              "deskryptorze! (WRITE)\n");
          FD_CLR(i, &allClientsForRead);
          writeStatus = 0;
        } else if (temporaryClient->name == NULL) {
          snprintf(messageBuffer, MESSAGE_SIZE,
                   "[server]>*Send me your login!*");
          writeStatus = writeMessage(i, messageBuffer);
          printf("Wyslano prosbe o login\n");
        } else {
          memset(messageBuffer, 0, MESSAGE_SIZE);
          if (temporaryClient->firstMessage) {
            memset(tempString, 0, MESSAGE_SIZE);

            getAllClientsFromRoom(clients, tempString, temporaryClient->room);
            snprintf(messageBuffer, MESSAGE_SIZE,
                     "[server]>*Hello on IRC, %s! You're in Main channel (Room "
                     "1). People online: %s! Type /help to get commands!*",
                     temporaryClient->name, tempString);

            temporaryClient->firstMessage = 0;
            writeStatus = writeMessage(i, messageBuffer);

          } else {
            if (temporaryClient->serverResponse != NULL) {
              writeStatus = writeMessage(i, temporaryClient->serverResponse);
              free(temporaryClient->serverResponse);
              temporaryClient->serverResponse = NULL;
            }
          }
        }

        FD_CLR(i, &clientsForWrite);

        if (!writeStatus) {
          printf("whhhhhattt1\n");
          FD_CLR(i, &allClientsForRead);
          removeClient(&temporaryClient, &clients);
          showClients(clients);
        }
      } else if (FD_ISSET(i, &rmask)) {
        memset(messageBuffer, 0, MESSAGE_SIZE);
        readStatus = readMessage(i, &messageBuffer);
        printf("->%s\n", messageBuffer);
        if (!readStatus) {
          memset(tempString, 0, MESSAGE_SIZE);
          snprintf(tempString, MESSAGE_SIZE,
                   "[server]> %s has left your channel!",
                   temporaryClient->name);

          sendMessageToRoom(clients, temporaryClient->room, tempString,
                            temporaryClient->connectionNumber);
          FD_CLR(i, &clientsForWrite);
          FD_CLR(i, &allClientsForRead);
          removeClient(&temporaryClient, &clients);
          showClients(clients);
          if (i == maxFileDescriptor)
            while (maxFileDescriptor > socketDescriptor &&
                   !FD_ISSET(maxFileDescriptor, &allClientsForRead))
              maxFileDescriptor -= 1;
        } else {
          if (temporaryClient == NULL) {
            printf(
                "WOW, cos poszlo nie tak! Nie ma takiego clienta po "
                "deskryptorze! (READ)\n");
            FD_CLR(i, &clientsForWrite);
            FD_CLR(i, &allClientsForRead);
            readStatus = 0;
          } else if (temporaryClient->name == NULL) {
            if (!findClientByName(clients, messageBuffer)) {
              printf("Wybrany nick: %s\n", messageBuffer);

              temporaryClient->name =
                  malloc((strlen(messageBuffer) + 1) * sizeof(char));
              strcpy(temporaryClient->name, messageBuffer);
              temporaryClient->room = MAIN_ROOM;
              temporaryClient->firstMessage = 1;

              memset(tempString, 0, MESSAGE_SIZE);
              snprintf(tempString, MESSAGE_SIZE,
                       "[server]> %s has joined your channel!",
                       temporaryClient->name);
              sendMessageToRoom(clients, temporaryClient->room, tempString,
                                temporaryClient->connectionNumber);
            }

            FD_SET(i, &clientsForWrite);
          } else {
            FD_SET(i, &clientsForWrite);
            memset(tempString, 0, MESSAGE_SIZE);
            if (isCommand(messageBuffer)) {
              strcpy(tempString, messageBuffer);
              char *roomName = strtok(tempString, " ");
              roomName = strtok(0, " ");
              tempServerResponse = malloc(sizeof(char) * MESSAGE_SIZE);

              if (!strcmp(tempString, "/help")) {
                strcpy(tempServerResponse,
                       "[server]>*Available commands:\n/help - gets you "
                       "help\n/who - tell you who is online\n/switch < room "
                       "name > - switch you to other room\n/room - tells you "
                       "in which room you are*\0");
              } else if (!strcmp(tempString, "/who")) {
                getAllClientsFromRoom(clients, messageBuffer,
                                      temporaryClient->room);
                snprintf(tempServerResponse, MESSAGE_SIZE,
                         "[server]>*People in room %d: %s*",
                         temporaryClient->room, messageBuffer);
              } else if (!strcmp(tempString, "/switch")) {
                int newRoom = atoi(roomName);
                int oldRoom = temporaryClient->room;
                if (newRoom != oldRoom) {
                  temporaryClient->room = newRoom;

                  snprintf(tempString, MESSAGE_SIZE,
                           "[server]> %s has left your channel!",
                           temporaryClient->name);

                  sendMessageToRoom(clients, oldRoom, tempString,
                                    temporaryClient->connectionNumber);

                  snprintf(tempString, MESSAGE_SIZE,
                           "[server]> %s has joined your channel!",
                           temporaryClient->name);
                  sendMessageToRoom(clients, newRoom, tempString,
                                    temporaryClient->connectionNumber);

                  snprintf(tempServerResponse, MESSAGE_SIZE,
                           "[server]>*You've changed your channel to: %d!*",
                           temporaryClient->room);
                } else {
                  snprintf(tempServerResponse, MESSAGE_SIZE,
                           "[server]>*You're already in room %d!*",
                           temporaryClient->room);
                }
              } else if (!strcmp(tempString, "/room")) {
                snprintf(tempServerResponse, MESSAGE_SIZE,
                         "[server]>*You're in room: %d*",
                         temporaryClient->room);
              } else {
                strcpy(tempServerResponse,
                       "[server]>*I don't understand this command!*\0");
              }

              temporaryClient->serverResponse = tempServerResponse;
            } else {
              snprintf(tempString, MESSAGE_SIZE, "[%s]> %s",
                       temporaryClient->name, messageBuffer);
              printf("%s\n", tempString);
              sendMessageToRoom(clients, temporaryClient->room, tempString,
                                temporaryClient->connectionNumber);
            }
          }
        }
      }
    }
  }

  close(socketDescriptor);
  return EXIT_SUCCESS;
}