#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/signal.h>

#define MESSAGE_SIZE 256

int getInput(char** buff) {
  char temp[MESSAGE_SIZE];
  int strLen;

  fgets(temp, MESSAGE_SIZE, stdin);
  strLen = strlen(temp) - 1;
  temp[strLen] = '\0';
  strcpy(*buff, temp);

  return strLen;
}
void childend(int signo) { wait(NULL); }
int readMessage(int fileDescriptorToReadFrom, char** pointerForMessage) {
  int totalReadLength = 0;
  int readLength = 0;

  while (totalReadLength < MESSAGE_SIZE) {
    readLength = read(fileDescriptorToReadFrom,
                      (*pointerForMessage) + totalReadLength, MESSAGE_SIZE);
    if (readLength <= 0) {
      return 0;
    }
    totalReadLength += readLength;
    if (strchr(*pointerForMessage, '\0')) break;
  }

  return 1;
}

int writeMessage(int fileDescriptorToWriteTo, char* messageToSend) {
  int currentMessageSize = strlen(messageToSend) + 1;
  if (MESSAGE_SIZE < currentMessageSize) currentMessageSize = MESSAGE_SIZE;

  int totalWroteLength = 0;
  int wroteLength = 0;

  while (totalWroteLength < MESSAGE_SIZE) {
    wroteLength = write(fileDescriptorToWriteTo,
                        messageToSend + totalWroteLength, MESSAGE_SIZE);
    totalWroteLength += wroteLength;
    if (wroteLength <= 0) {
      return 0;
    }
  }
  printf("sent: %s\n", messageToSend);
  return 1;
}
void* reader(void* arg) {
  int* socketDescriptor = (int*)arg;
  char* msg = malloc(MESSAGE_SIZE * sizeof(char));
  while (1) {
    memset(msg, 0, MESSAGE_SIZE);
    readMessage(*socketDescriptor, &msg);
    if (strlen(msg) > 1) printf("%s\n", msg);
  }
  return EXIT_SUCCESS;
}

// Client application
int main(int argc, char** argv) {
  struct sockaddr_in addr;
  struct hostent* host = gethostbyname(argv[1]);
  pthread_t tid;

  addr.sin_family = PF_INET;
  addr.sin_port = htons(atoi(argv[2]));
  memcpy(&addr.sin_addr.s_addr, host->h_addr, host->h_length);

  int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);
  connect(socketDescriptor, (struct sockaddr*)&addr, sizeof(addr));
  signal(SIGCHLD, childend);

  int readStatus;
  char* msg = malloc(MESSAGE_SIZE * sizeof(char));
  pthread_create(&tid, NULL, reader, &socketDescriptor);
  pthread_detach(tid);

  if (argc == 4) {
    memset(msg, 0, MESSAGE_SIZE);
    write(socketDescriptor, argv[3], strlen(argv[3]));
  }

  while (1) {
    memset(msg, 0, MESSAGE_SIZE);
    readStatus = getInput(&msg);
    write(socketDescriptor, msg, readStatus);
  }
  close(socketDescriptor);

  return 0;
}
